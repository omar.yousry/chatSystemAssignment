# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Application, type: :model do
  describe 'valid' do
    let(:application) { create :application }
    it 'has a valid factory' do
      expect(application).to be_valid
    end
  end

  describe 'uniqueness' do
    context 'validate uniqueness' do
      let!(:application) { create(:application, name: 'some_name', token: 'not_a_unique_token') }
      let(:duplicate_token) { create(:application, name: 'new_name', token: 'not_a_unique_token') }
      let(:duplicate_name) { create(:application, name: 'some_name', token: 'another_unique_token') }
      let(:different) { create(:application, name: 'another_name', token: 'a_unique_token') }

      it 'should not allow duplicate applications' do
        expect { duplicate_token }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it 'should not allow duplicate applications' do
        expect { duplicate_name }.to raise_error(ActiveRecord::RecordInvalid)
      end

      it 'should allow unique applications' do
        expect { different }.to change { Application.count }.by(1)
      end
    end
  end
end
