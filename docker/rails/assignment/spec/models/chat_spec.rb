require 'rails_helper'

RSpec.describe Chat, type: :model do
  describe 'valid' do
    let(:chat) { create :chat }
    it 'has a valid factory' do
      expect(chat).to be_valid
    end
  end

  describe 'uniqueness' do
    context 'validate uniqueness' do
      let!(:application) { create(:application, name: 'test_app', token: 'unique_token') }
      let!(:new_application) { create(:application, name: 'another_test_app', token: 'unique_token_2') }
      let(:uniq_chat) { create(:chat, application: application, number: 1) }
      let(:dup_chat) { create(:chat, application: application, number: 1) }
      let(:another_uniq_chat) { create(:chat, application: new_application, number: 1) }

      it 'should accept and reject' do
        expect { uniq_chat }.to change { application.chats.count }.by(1)
        expect { dup_chat }.to raise_error(ActiveRecord::RecordInvalid)
      end
      it 'should accept new inc chat' do
        expect { another_uniq_chat }.to change { new_application.chats.count }.by(1)
      end
    end
  end
end
