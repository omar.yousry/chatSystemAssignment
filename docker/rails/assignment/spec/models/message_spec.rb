# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Message, type: :model do
  describe 'valid' do
    let(:message) { create :message }
    it 'has a valid factory' do
      expect(message).to be_valid
    end
  end

  describe 'uniqueness' do
    context 'validate uniqueness' do
      let!(:application) { create(:application, name: 'test_app', token: 'unique_token') }
      let(:chat) { create(:chat, application: application) }
      let(:new_chat) { create(:chat, application: application) }
      let(:new_msg) { create(:message, chat: chat, number: 1) }
      let(:dup_msg) { create(:message, chat: chat, number: 1) }
      let(:another_uniq_msg) { create(:message, chat: chat, number: 2) }
      let(:new_chat_msg) { create(:message, chat: new_chat) }

      it 'should accept and reject' do
        expect { new_msg }.to change { chat.messages.count }.by(1)
        expect { dup_msg }.to raise_error(ActiveRecord::RecordInvalid)
        expect { another_uniq_msg }.to change { chat.messages.count }.by(1)
      end

      it 'should accept new inc chat' do
        expect { new_chat_msg }.to change { new_chat.messages.count }.by(1)
      end
    end
  end
end
