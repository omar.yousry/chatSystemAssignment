FactoryGirl.define do
  factory :message do
    chat factory: :chat
    body { Faker::Lorem.sentence }
  end
end
