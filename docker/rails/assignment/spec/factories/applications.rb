FactoryGirl.define do
  factory :application do
    name     { Faker::Name.name }
    token    { Faker::Crypto.md5 }
  end
end
