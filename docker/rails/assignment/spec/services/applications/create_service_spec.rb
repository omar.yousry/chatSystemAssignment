RSpec.describe Applications::CreateService do
  describe '#create' do
    context 'valid create' do
      let(:service_args) do
        {
          name: 'TestApp'
        }
      end

      context 'new application' do
        it 'creates the message and the chat room' do
          allow(Application).to receive(:find_by).and_return(nil)

          expect do
            Applications::CreateService.new(service_args).execute
          end.to change { Application.count }.by(1)

          expect do
            Applications::CreateService.new(service_args).execute
          end.to change { Application.count }.by(0)
        end
      end

      context 'with race conditions' do
        threads = []

        it 'handle concurrent requests' do
          expect do
            3.times do |i|
              args = { name: "TestApp_#{i}" }
              threads << Thread.new do
                Applications::CreateService.new(args).execute
              end
            end
            threads.map(&:join)
          end.not_to raise_exception

          expect(Application.all.count).to equal(3)
          expect(Application.all.pluck(:token).uniq.count).to equal(3)
        end
      end
    end
  end
end
