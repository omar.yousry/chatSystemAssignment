RSpec.describe Chats::CreateService do
  describe '#create' do
    context 'valid create' do
      let(:application) { create :application }

      context 'new chat' do
        it 'creates the message and the chat room' do
          allow(Chat).to receive(:find_by).and_return(nil)

          expect do
            Chats::CreateService.new(application).execute
          end.to change { application.chats.count }.by(1)
        end
      end
    end
  end

  describe 'number uniqueness' do
    context 'validate uniqueness' do
      let(:new_application) { create(:application) }

      it 'should allow unique applications' do
        expect { new_application }.to change { Application.count }.by(1)
      end
    end

    context 'with race conditions' do
      let(:new_application) { create(:application) }
      threads = []

      it 'handle concurrent requests' do
        expect do
          3.times do |_i|
            threads << Thread.new do
              Chats::CreateService.new(new_application).execute
            end
          end
          threads.map(&:join)
        end.not_to raise_exception

        expect(new_application.chats.count).to equal(3)
        expect(new_application.chats.pluck(:number).uniq.count).to equal(3)
      end
    end
  end
end
