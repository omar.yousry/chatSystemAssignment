RSpec.describe Messages::CreateService do
  let(:message_params) do
    {
      body: 'message sent'
    }
  end
  describe '#create' do
    context 'valid create' do
      let(:application) { create :application }
      let(:chat) { create :chat,  application: application }

      context 'new message' do
        it 'creates the message and the chat room' do
          allow(Message).to receive(:find_by).and_return(nil)

          expect do
            Messages::CreateService.new(chat, message_params).execute
          end.to change { chat.messages.count }.by(1)
        end
      end
    end
  end

  describe 'number uniqueness' do
    context 'with race conditions' do
      let(:new_chat) { create(:chat) }
      threads = []

      it 'handle concurrent requests' do
        expect do
          4.times do |_i|
            threads << Thread.new do
              Messages::CreateService.new(new_chat, message_params).execute
            end
          end
          threads.map(&:join)
        end.not_to raise_exception

        expect(new_chat.messages.count).to equal(4)
        expect(new_chat.messages.pluck(:number).uniq.count).to equal(4)
      end
    end
  end
end
