class ApplicationsController < ApplicationController
  before_action :set_applications, only: [:index]
  before_action :set_application, only: [:show]

  def index
    render json: @applications
  end

  def show
    render json: @application
  end

  def create
    application = Applications::CreateService.new(application_params).execute
    if application.persisted?
      render json: application, status: :created, serializer: ApplicationSerializer
    else
      render json: { errors: application.errors }, status: :unprocessable_entity
    end
  end

  private

  def application_params
    params.require(:application).permit(:name)
  end

  def set_applications
    @applications = Application.all
  end

  def set_application
    @application = Application.find_by(token: params[:id])
  end
end
