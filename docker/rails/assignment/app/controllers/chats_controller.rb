class ChatsController < ApplicationController
  before_action :set_application
  before_action :set_chats, only: [:index]
  before_action :set_chat, only: %i[show messages_search]

  def index
    render json: @chats
  end

  def show
    render json: @chat
  end

  def messages_search
    result = Chats::SearchService.new(@chat, params[:q]).execute
    render json: result
  end

  def create
    chat_queue = Redis::Semaphore.new(:chat_semaphore, host: request.host, stale_client_timeout: 2)
    chat_queue.lock do
      chat = Chats::CreateService.new(@application).execute
      if chat.persisted?
        render json: chat, status: :created, serializer: ChatSerializer
      else
        render json: { errors: chat.errors }, status: :unprocessable_entity
      end
    end
  end

  private

  def set_chats
    @chats = @application.chats
  end

  def set_chat
    @chat = @application.chats.find_by(number: params[:id] || params[:chat_id])
  end

  def set_application
    @application = Application.find_by(token: params[:application_id])
  end
end
