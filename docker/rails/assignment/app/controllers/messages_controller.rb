class MessagesController < ApplicationController
  before_action :message_stack
  before_action :set_message, only: [:show]
  before_action :set_messages, only: [:index]

  def index
    render json: @messages
  end

  def show
    render json: @message
  end

  def create
    message_queue = Redis::Semaphore.new(:message_semaphore, host: request.host, stale_client_timeout: 2)
    message_queue.lock do
      message = Messages::CreateService.new(@chat, message_params).execute
      if message.persisted?
        render json: message, status: :created, serializer: MessageSerializer
      else
        render json: { errors: message.errors }, status: :unprocessable_entity
      end
    end
  end

  private

  def message_params
    params.require(:message).permit(:body)
  end

  def message_stack
    set_application
    set_chat
  end

  def set_application
    @application = Application.find_by(token: params[:application_id])
  end

  def set_chat
    @chat = @application.chats.find_by(number: params[:chat_id])
  end

  def set_message
    @message = @chat.messages.find_by(number: params[:id])
  end

  def set_messages
    @messages = @chat.messages
  end
end
