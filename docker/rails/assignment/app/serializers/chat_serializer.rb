class ChatSerializer < ActiveModel::Serializer
  attributes :number, :messages_count, :created_at
  attribute :application_token

  has_many :messages, serializer: MessageSerializer

  def application_token
    @object.application_token
  end
end
