class ApplicationSerializer < ActiveModel::Serializer
  attributes :name, :token, :chat_count, :created_at
  has_many :chats, serializer: ChatSerializer
end
