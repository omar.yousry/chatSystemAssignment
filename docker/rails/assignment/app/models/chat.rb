class Chat < ApplicationRecord
  # Validation #
  validates :number, presence: true, uniqueness: { scope: :application_id }

  # Actions #
  before_validation :assign_number
  after_create :update_application_chat_count

  # Depenecides #
  belongs_to :application
  has_many :messages, dependent: :destroy

  def assign_number
    Chat.transaction do
      app = Application.lock.find_by(id: application_id)
      self.number ||= app.chats.count + 1
    end
  end

  def update_application_chat_count
    ApplicationIndexerJob.perform_async(application_id)
  end

  def application_token
    application.token
  end
end
