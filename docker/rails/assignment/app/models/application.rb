class Application < ApplicationRecord
  include UtilityConcern

  # Validation #
  validates :name, :token, presence: true, uniqueness: { case_sensitive: false }

  # Actions #
  before_validation :create_token

  # Depenecides #
  has_many :chats, dependent: :destroy

  def create_token
    self.token ||= generate_token
  end

  def generate_token
    loop do
      token = "#{random_token}#{unique_ending}"
      break token unless Application.exists?(token: token)
    end
  end
end
