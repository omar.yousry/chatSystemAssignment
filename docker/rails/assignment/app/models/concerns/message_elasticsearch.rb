module MessageElasticsearch
  extend ActiveSupport::Concern

  included do
    include ActionView::Helpers::AssetUrlHelper
    include Elasticsearch::Model
    include Elasticsearch::Model::Callbacks

    index_name Rails.env.to_s
    document_type 'messages_analyzer_index'

    ES_SETTINGS = {
      "index": {
        "analysis": {
          "filter": {
            "autocomplete_filter": {
              "type": 'edge_ngram',
              "min_gram": 1,
              "max_gram": 20
            }
          },
          "analyzer": {
            "autocomplete": {
              "type": 'custom',
              "tokenizer": 'whitespace',
              "normalizer": 'lowercase_normalizer',
              "filter": %w[
                lowercase
                autocomplete_filter
              ]
            },
            "messages_analyzer": {
              "type": 'custom',
              "tokenizer": 'whitespace',
              "filter": [
                'lowercase'
              ]
            }
          }
        }
      }
    }

    settings ES_SETTINGS do
      mapping dynamic: 'false' do
        indexes :body, type: 'text', fields: { autocomplete: { type: 'text', analyzer: 'autocomplete', search_analyzer: 'messages_analyzer' } }
        indexes :chat_id, type: 'integer'
      end
    end

    def as_indexed_json(_options = {})
      as_json(
        only: %i[id number body chat_id created_at],
        include: {
          chat: { only: %i[id number] }
        }
      )
    end

    after_commit on: %i[create] do
      SearchIndexerJob.perform_async(self, :index)
    end
  end
end
