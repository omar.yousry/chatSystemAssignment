class Message < ApplicationRecord
  include MessageElasticsearch

  # Validation #
  validates :body, presence: true
  validates :number, presence: true, uniqueness: { scope: :chat_id }

  # Actions #
  before_validation :assign_number
  after_create :update_chat_messages_count

  # Depenecides #
  belongs_to :chat

  def assign_number
    Message.transaction do
      mod_chat = Chat.lock.find_by(id: chat_id)
      self.number ||= mod_chat.messages.count + 1
    end
  end

  def update_chat_messages_count
    ChatIndexerJob.perform_async(chat_id)
  end
end
