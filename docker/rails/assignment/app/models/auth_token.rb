class AuthToken < ApplicationRecord
  include UtilityConcern

  # Scope #
  scope :active, -> { where(expired: false) }

  # Validation #
  validates :token, presence: true, uniqueness: { case_sensitive: false }

  # Actions #
  before_validation :create_token
  after_create :expire_token

  def create_token
    self.token ||= generate_token
  end

  def generate_token
    loop do
      token = "#{random_token}#{unique_ending}"
      break token unless AuthToken.exists?(token: token)
    end
  end

  def expire_token
    ExpireTokenJob.perform_in(2.hours, id)
  end
end
