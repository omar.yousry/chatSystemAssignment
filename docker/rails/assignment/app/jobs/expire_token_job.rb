class ExpireTokenJob
  include Sidekiq::Worker

  def perform(id)
    return if Rails.env.test?

    auth_token = AuthToken.find(id)
    auth_token.update(expired: true)
  end
end
