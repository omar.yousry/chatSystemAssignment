class ApplicationIndexerJob
  include Sidekiq::Worker

  def perform(id)
    return if Rails.env.test?

    application = Application.find(id)
    application.update(chat_count: application.chats.count + 1)
  end
end
