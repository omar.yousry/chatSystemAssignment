class ChatIndexerJob
  include Sidekiq::Worker

  def perform(id)
    return if Rails.env.test?

    chat = Chat.find(id)
    chat.update(messages_count: chat.messages.count + 1)
  end
end
