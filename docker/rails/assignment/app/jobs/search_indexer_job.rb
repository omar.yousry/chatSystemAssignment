class SearchIndexerJob
  include Sidekiq::Worker

  def perform(record, action)
    return if Rails.env.test?

    record.__elasticsearch__.index_document if action == :index
  end
end
