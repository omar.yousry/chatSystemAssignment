module Applications
  class CreateService < BaseService
    attr_accessor :params

    def initialize(params)
      @params = params
    end

    def _execute
      application = Application.new(params)

      if application.save
        # Add some logic here #
      end
      application
    end
  end
end
