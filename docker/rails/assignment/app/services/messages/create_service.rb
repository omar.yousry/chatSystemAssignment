module Messages
  class CreateService < BaseService
    attr_accessor :chat, :message_params

    def initialize(chat, message_params)
      @chat = chat
      @message_params = message_params
    end

    def _execute
      message = Message.new(message_params.merge(chat_id: chat.id))
      if message.save
        # Add some logic here #
      end
      message
    end
  end
end
