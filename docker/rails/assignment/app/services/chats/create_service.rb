module Chats
  class CreateService < BaseService
    attr_accessor :application

    def initialize(application)
      @application = application
    end

    def _execute
      chat = Chat.new(application_id: application.id)
      if chat.save
        # Add some logic here #
      end
      chat
    end
  end
end
