# frozen_string_literal: true

module Chats
  class SearchService < BaseService
    attr_accessor :chat, :full_text

    def initialize(chat, full_text)
      @chat = chat
      @full_text = full_text
    end

    def _execute
      search_results = Message.__elasticsearch__.search(query_scheme)

      class << search_results
        def auto_complete_suggestions
          results.map { |result| result.body.to_s }.uniq
        end
      end

      extract_results_from(search_results)
    end

    def query_scheme
      {
        query: {
          bool: {
            filter: {
              term: { chat_id: chat.id }
            },
            must: {
              multi_match: {
                query: full_text,
                fields: ['body.autocomplete']
              }
            }
          }
        }
      }
    end

    def extract_results_from(search_results)
      begin
        results = search_results.results.results
      rescue Elasticsearch::Transport::Transport::Error => e
        raise e
      end
      messages = prepare_messages(results)
      {
        messages: messages,
        suggestions: search_results.auto_complete_suggestions.map { |suggestion| { name: suggestion } },
        total_count: search_results.count
      }
    end

    def prepare_messages(results)
      message_ids = chat.messages.pluck(:id)
      results.select! { |result| message_ids.include?(result['_source']['id']) }
      results.map do |result|
        Message.find_by(id: result['_source']['id'])
      end
    end
  end
end
