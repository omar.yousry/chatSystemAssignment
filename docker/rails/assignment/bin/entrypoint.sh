#!/bin/bash
set -e

[[ $DEBUG == true ]] && set -x

RAILS_APP_PATH="$( cd "$(dirname "$0")" ; cd .. ; pwd -P )"
cd $RAILS_APP_PATH

echo "Running > ${1}..."
bundle install
bundle exec rake db:migrate 2>/dev/null || bundle exec rake db:create db:migrate
bundle exec sidekiq -d -L log/sidekiq.log -C config/sidekiq.yml
rails s -p 3000 -b 0.0.0.0

exec "$@"
