# frozen_string_literal: true

Message.__elasticsearch__.create_index! force: true
Message.__elasticsearch__.refresh_index!
Message.all.each do |record|
  record.__elasticsearch__.index_document
end
