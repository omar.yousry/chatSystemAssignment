class AddMessages < ActiveRecord::Migration[5.2]
  def change
    create_table :messages do |t|
      t.integer :number
      t.string :body
      t.references :chat, index: true
      t.timestamps
    end
  end
end
