class AddAuthToken < ActiveRecord::Migration[5.2]
  def change
    create_table :auth_tokens do |t|
      t.string :token
      t.boolean :expired, default: false
      t.timestamps
    end
  end
end
