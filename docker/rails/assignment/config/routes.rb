Rails.application.routes.draw do
  resources :auth_tokens, only: [:create]
  post 'authenticate' => 'auth_tokens#create'

  resources :applications do
    resources :chats do
      get :messages_search
      resources :messages
    end
  end
end
