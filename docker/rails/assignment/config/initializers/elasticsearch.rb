config = {
  host: Settings.elasticsearch.host,
  transport_options: {
    request: { timeout: Settings.elasticsearch.timeout }
  },
  log: Settings.elasticsearch.log

}

Elasticsearch::Model.client = Elasticsearch::Client.new(config)
