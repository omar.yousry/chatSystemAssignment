require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module ChatSystemAssignment
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2
    config.action_dispatch.default_headers = {
      'X-Frame-Options' => 'ALLOWALL',
      'Access-Control-Expose-Headers' => 'X-Request-Id'
    }
    config.middleware.insert_before 0, Rack::Cors do
      allow do
        origins '*'
        resource '*',
                 headers: :any,
                 methods: %i[get post options delete head patch],
                 expose: %w[access-token expiry token-type uid client X-Request-Id]
      end
    end

    config.active_job.queue_adapter = :sidekiq
  end
end
