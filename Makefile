build:
	docker build . -t chatsystemassignment:debug

compose_up:
	docker-compose -f docker-compose.yml -p chatsystemassignment up

compose_down:
	docker-compose -f docker-compose.yml -p chatsystemassignment down