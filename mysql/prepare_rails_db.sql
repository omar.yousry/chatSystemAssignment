CREATE DATABASE IF NOT EXISTS `chat_system_dev`;
CREATE DATABASE IF NOT EXISTS `chat_system_test`;

CREATE USER 'chat_system_dev'@'%' IDENTIFIED BY 'chat_system_dev';
GRANT ALL ON `chat_system_dev`.* TO 'chat_system_dev'@'%';

CREATE USER 'chat_system_test'@'%' IDENTIFIED BY 'chat_system_test';
GRANT ALL ON `chat_system_test`.* TO 'chat_system_test'@'%';

FLUSH PRIVILEGES;
