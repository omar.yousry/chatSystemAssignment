source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.0'

gem 'mysql2', '>= 0.4.4', '< 0.6.0'
gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.0'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'

gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'turbolinks', '~> 5'

gem 'bootsnap', '>= 1.1.0', require: false
group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_girl_rails', '~> 4.5.0'
  gem 'pry-byebug', '~> 3.6'
  gem 'pry-rails'
  gem 'rspec-rails', '~> 3.6'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15', '< 4.0'
  gem 'chromedriver-helper'
  gem 'database_cleaner', '~> 1.5'
  gem 'faker'
  gem 'selenium-webdriver'
  gem 'shoulda-matchers', '~> 3.0', require: false
end

gem 'active_model_serializers', '~> 0.10.0'
gem 'config'
gem 'elasticsearch-model'
gem 'elasticsearch-rails'
gem 'rack-cors'
gem 'rubocop'
gem 'sidekiq'
gem 'redis-semaphore'

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
