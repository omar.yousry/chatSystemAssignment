package main

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
)

func handler(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}
	query := r.URL.Query()
	header := r.Header.Get("Authorization-Key")
	req := query.Get("req")

	reqBody, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
	}

	dataMap := map[string]interface{}{}
	if err := json.Unmarshal([]byte(reqBody), &dataMap); err != nil {
		panic(err)
	}

	host := dataMap["host"].(string)

	switch req {
	case "application":
		name := dataMap["name"].(string)

		requestBody, err := json.Marshal(map[string]string{
			"name": name,
		})
		if err != nil {
			log.Fatal(err)
		}

		url := "http://" + host + ":3000/applications/"

		req, _ := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))

		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Authorization-Key", header)

		res, _ := http.DefaultClient.Do(req)

		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)

		fmt.Println(res)
		fmt.Println(string(body))
	case "chat":

		applicationToken := dataMap["application_token"].(string)

		requestBody, err := json.Marshal(map[string]string{})
		if err != nil {
			log.Fatal(err)
		}

		url := "http://" + host + ":3000/applications/" + applicationToken + "/chats"

		req, _ := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))

		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Authorization-Key", header)

		res, _ := http.DefaultClient.Do(req)

		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)

		fmt.Println(res)
		fmt.Println(string(body))
	case "message":
		applicationToken := dataMap["application_token"].(string)
		chatNumber := dataMap["chat_number"].(string)
		content := dataMap["content"].(string)

		requestBody, err := json.Marshal(map[string]string{
			"body": content,
		})
		if err != nil {
			log.Fatal(err)
		}
		url := "http://" + host + ":3000/applications/" + applicationToken + "/chats/" + chatNumber + "/messages"

		req, _ := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))

		req.Header.Add("Content-Type", "application/json")
		req.Header.Add("Authorization-Key", header)

		res, _ := http.DefaultClient.Do(req)

		defer res.Body.Close()
		body, _ := ioutil.ReadAll(res.Body)

		fmt.Println(res)
		fmt.Println(string(body))
	default:
		w.WriteHeader(http.StatusNotImplemented)
		w.Write([]byte(http.StatusText(http.StatusNotImplemented)))
	}
}

func main() {
	// Create Server and Route Handlers
	r := mux.NewRouter()

	r.HandleFunc("/", handler)

	srv := &http.Server{
		Handler:      r,
		Addr:         ":8080",
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	// Start Server
	go func() {
		log.Println("Starting Server")
		if err := srv.ListenAndServe(); err != nil {
			log.Fatal(err)
		}
	}()

	// Graceful Shutdown
	waitForShutdown(srv)
}

func waitForShutdown(srv *http.Server) {
	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	// Block until we receive our signal.
	<-interruptChan

	// Create a deadline to wait for.
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	srv.Shutdown(ctx)

	log.Println("Shutting down")
	os.Exit(0)
}
